package com.example.allinone.splashScrren;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.example.allinone.R;


public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    //////////// Arrays ///////////

    private int[] slider_image = {

            R.drawable.ic_backup_black_24dp,
            R.drawable.ic_blur_on_black_24dp,
            R.drawable.ic_gesture_black_24dp,
            R.drawable.ic_favorite_black_24dp

    };

    private String[] slider_title = {

            "title1",
            "title2",
            "title3",
            "title4"
    };

    private String[] slider_desc = {

            "Description1",
            "Description2",
            "Description3",
            "Description4"
    };

    @Override
    public int getCount() {
        return slider_title.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider, container, false);

        ImageView s_image = view.findViewById(R.id.s_img);
        TextView s_title = view.findViewById(R.id.s_tit);
        TextView s_desc = view.findViewById(R.id.s_desc);

        s_image.setImageResource(slider_image[position]);
        s_title.setText(slider_title[position]);
        s_desc.setText(slider_desc[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
