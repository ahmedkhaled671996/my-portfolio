package com.example.allinone.allUi.myUI.rxWithRoom.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface UserDeo {

    @Query("SELECT * FROM users Where id=:userId")
    Flowable<RoomUser> getUserById(String userId);

    @Query("SELECT * FROM users")
    Flowable<List<RoomUser>> getAllUsers();

    @Insert
    void insertUser(RoomUser... users);

    @Update
    void updateUser(RoomUser... users);

    @Delete
    void deleteUser(RoomUser user);

    @Query("Delete FROM users")
    void deleteAllUsers();
}
