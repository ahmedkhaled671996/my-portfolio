package com.example.allinone.allUi.myUI.sQLiteDataBase.models;

public class SQLiteTestModel {

    private String TeastId = null;
    private String TeastData = null;

    public SQLiteTestModel(String teastId, String teastData) {
        TeastId = teastId;
        TeastData = teastData;
    }

    public SQLiteTestModel() {

    }

    public String getTeastId() {
        return TeastId;
    }

    public void setTeastId(String teastId) {
        TeastId = teastId;
    }

    public String getTeastData() {
        return TeastData;
    }

    public void setTeastData(String teastData) {
        TeastData = teastData;
    }
}
