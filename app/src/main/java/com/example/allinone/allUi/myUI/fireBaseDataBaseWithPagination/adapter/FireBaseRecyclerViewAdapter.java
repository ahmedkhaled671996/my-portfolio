package com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.myInterface.ILoadMore;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.Models.MyFireBaseItem;
import com.example.allinone.R;
import com.example.allinone.customView.CustomTextView;

import java.util.List;

class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public LoadingViewHolder(@NonNull View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.firebaseProgressBar);

    }

}

class ItemViewHolder extends RecyclerView.ViewHolder {

    public CustomTextView itemID, itemName;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        itemID = itemView.findViewById(R.id.firebase_item_id_TV);
        itemName = itemView.findViewById(R.id.firebase_item_name_TV);

    }

}

public class FireBaseRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    ILoadMore iLoadMore;
    boolean isLoading;
    AppCompatActivity activity;
    List<MyFireBaseItem> myFireBaseItems;
    int VTH = 5, lastVisibleItem, totalItemCount;

    public FireBaseRecyclerViewAdapter(RecyclerView recyclerView, AppCompatActivity activity, List<MyFireBaseItem> myFireBaseItems) {
        this.activity = activity;
        this.myFireBaseItems = myFireBaseItems;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + VTH)) {
                    if (iLoadMore != null) {
                        iLoadMore.onLoadMore();
                        isLoading = true;
                    }

                }
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return myFireBaseItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setiLoadMore(ILoadMore iLoadMore) {
        this.iLoadMore = iLoadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_firebase_recyclerview, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            MyFireBaseItem myFireBaseItem = myFireBaseItems.get(position);
            ItemViewHolder viewHolder = (ItemViewHolder) holder;
            viewHolder.itemName.setText(myFireBaseItem.getItemName());
            viewHolder.itemID.setText(myFireBaseItem.getItemID());

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return myFireBaseItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }
}
