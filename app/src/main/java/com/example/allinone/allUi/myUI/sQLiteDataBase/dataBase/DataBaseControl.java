package com.example.allinone.allUi.myUI.sQLiteDataBase.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.allinone.allUi.myUI.sQLiteDataBase.models.SQLiteTestModel;

import java.util.ArrayList;

public class DataBaseControl {

    DataBase dbinfo;
    Context context;

    public DataBaseControl(Context context) {
        dbinfo = new DataBase(context);
        this.context = context;
    }


    //////////////////////////////////////// insert data ///////////////////////////////////////////////////////////////////
    public void insert_Test_data(String TestID, String TestData) {
        SQLiteDatabase sqLiteDatabase = dbinfo.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(DataBase.TestID, TestID);
        contentValues.put(DataBase.TestData, TestData);

        long id = sqLiteDatabase.insert(DataBase.TestTABLE, null, contentValues);

    }

    /////////////////////////////////////// view data  ////////////////////////////////////////////////////////////////////
    public ArrayList<SQLiteTestModel> get_All_Data() {
        SQLiteDatabase sqLiteDatabase = dbinfo.getWritableDatabase();
        String[] coulm = {DataBase.TestID, DataBase.TestData};
        Cursor cursor = sqLiteDatabase.query(DataBase.TestTABLE, coulm, null, null, null, null, null);
        ArrayList<SQLiteTestModel> getAllData = new ArrayList<SQLiteTestModel>();
        while (cursor.moveToNext()) {

            getAllData.add(new SQLiteTestModel(
                    cursor.getString(0),
                    cursor.getString(1)
            ));

        }
        return getAllData;
    }

    //////////////////////////////////////// DeleteData  //////////////////////////////////////////////////
    public int deleteData(String SelectedData) {
        SQLiteDatabase sqLiteDatabase = dbinfo.getWritableDatabase();
        String[] whereArgs = {SelectedData};

        int count = sqLiteDatabase.delete(DataBase.TestTABLE, DataBase.TestID + " =? ", whereArgs);

        return count;

    }

    public void deleteAllData() {
        SQLiteDatabase sqLiteDatabase = dbinfo.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM " + DataBase.TestTABLE);
    }

    ////////////////////////////////////////updateData////////////////////////////////////////
    public int updateData(String oldeDataId, String newData) {
        SQLiteDatabase sqLiteDatabase = dbinfo.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBase.TestData, newData);
        String[] whereArgs = {oldeDataId};

        int count = sqLiteDatabase.update(DataBase.TestTABLE, contentValues, DataBase.TestID + " =? ", whereArgs);

        Toast.makeText(context, "updated", Toast.LENGTH_LONG).show();

        return count;

    }

    /***************************************************************************************/

    static class DataBase extends SQLiteOpenHelper {

        private Context context;
        private static final String dataBase_Name = "TestDataBase";
        /*********************************************************************/
        private static final String TestTABLE = "Test_table";
        /*********************************************************************/
        private static final int dataBase_version = 1;
        /*********************************************************************/
        private static final String TestID = "TestId";
        private static final String TestData = "TestData";
        /*********************************************************************/
        private static final String DROP_Test_TABLE = "DROP TABLE IF EXISTS " + TestTABLE;
        /***************************************************************************************/
        private static final String CREATE_Test_TABLE = "CREATE TABLE " + TestTABLE +
                " ( "
                + TestID + " VARCHAR(10), "
                + TestData + " VARCHAR(100) "
                + ");";

        /*************************************************************************************/


        public DataBase(Context context) {
            super(context, dataBase_Name, null, dataBase_version);
            this.context = context;

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_Test_TABLE);
                Toast.makeText(context, "on Create SQLite table", Toast.LENGTH_LONG).show();

            } catch (SQLException e) {
                Toast.makeText(context, "due to:" + e, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_Test_TABLE);
                onCreate(db);
                Toast.makeText(context, "onupgrade :", Toast.LENGTH_LONG).show();

            } catch (SQLException e) {
                Toast.makeText(context, "due to:" + e, Toast.LENGTH_LONG).show();
            }

        }

    }


}
