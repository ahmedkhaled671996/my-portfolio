package com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.Models.MyFireBaseItem;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter.FireBaseAdapter;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter.FireBaseRecyclerViewAdapter;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter.SweperAdapter;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.myInterface.ILoadMore;
import com.example.allinone.allUi.myUI.retrofit.helper.EndlessRecyclerViewScrollListener;
import com.example.allinone.hellper.RecyclerItemTouchHelper;
import com.example.allinone.hellper.RecyclerItemTouchHelperListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class FireBaseDataBaseWithPaginationFragment extends Fragment {

    View root;

    List<MyFireBaseItem> myFireBaseItems = new ArrayList<>();
    FireBaseRecyclerViewAdapter adapter;
    FireBaseAdapter fireBaseAdapter;
    RecyclerView recyclerView;
    DividerItemDecoration dividerItemDecoration;
    LinearLayoutManager linearLayoutManager;
    CoordinatorLayout rootlayout;
    SweperAdapter sweperAdapter;


    final int ILC = 20;
    int totalItems = 0, lastVisibleItem = 0;
    boolean isLoading = false, isMaxData = false;
    String lastNode = "", lastKey = "";
    Snackbar snackbar;

    @SuppressLint("ResourceType")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_firebase_database_with_pagination, container, false);

        Snackbar.make(Objects.requireNonNull(Objects.requireNonNull(getActivity()).getCurrentFocus()),
                getString(R.string.fireBaseDataBaseWithPaginationFragmentInfo), Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();

        getLastKey();

        recyclerView = root.findViewById(R.id.firebaseRecyclerView);
        sweperAdapter = new SweperAdapter(getContext(), myFireBaseItems);
        rootlayout = (CoordinatorLayout) root.findViewById(R.layout.fragment_firebase_database_with_pagination);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(sweperAdapter);

        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, new RecyclerItemTouchHelperListener() {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
                if (viewHolder instanceof SweperAdapter.MyviewHolder) {
                    String name = myFireBaseItems.get(viewHolder.getAdapterPosition()).getItemName();

                    final MyFireBaseItem fireBaseItem = myFireBaseItems.get(viewHolder.getAdapterPosition());
                    final int deleteIndex = viewHolder.getAdapterPosition();

                    sweperAdapter.removeItem(deleteIndex);

                    Snackbar snackbar = Snackbar.make(root, name + " removed", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sweperAdapter.restoreItem(fireBaseItem, deleteIndex);
                        }
                    });
                    snackbar.show();

                }
            }
        });
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerView);

        getData();


        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Toast.makeText(getContext(), "onLoadMore", Toast.LENGTH_SHORT).show();
                Query query = FirebaseDatabase
                        .getInstance()
                        .getReference()
                        .child("data")
                        .orderByChild("ItemName")
                        .limitToFirst(30);

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChildren()) {

                            List<MyFireBaseItem> newMyFireBaseItems = new ArrayList<>();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                newMyFireBaseItems.add(snapshot.getValue(MyFireBaseItem.class));
                            }
                            sweperAdapter.addAll(newMyFireBaseItems);
//                            Toast.makeText(getContext(), ""+fireBaseAdapter.getItemCount(), Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        isLoading = false;
                        Snackbar.make(root, "Error : " + databaseError.getMessage(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });
            }
        });


        return root;
    }

    public void ahmed() {
        for (int i = 0; i < 50; i++) {
            myFireBaseItems.add(new MyFireBaseItem(
                    "" + i, "user : " + i
            ));
        }
    }

    private void getData() {

        Query query = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("data")
                .orderByChild("ItemName")
                .limitToFirst(ILC);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChildren()) {

                    List<MyFireBaseItem> newMyFireBaseItems = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        newMyFireBaseItems.add(snapshot.getValue(MyFireBaseItem.class));
                    }
                    sweperAdapter.addAll(newMyFireBaseItems);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                isLoading = false;
                Snackbar.make(root, "Error : " + databaseError.getMessage(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    private void getLastKey() {
        Query query = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("data")
                .orderByKey()
                .limitToLast(1);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    lastKey = snapshot.getKey();
                    lastNode = snapshot.getValue(MyFireBaseItem.class).getItemName();
                    Toast.makeText(getContext(), lastNode, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Snackbar.make(root, "Error : " + databaseError.getMessage(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void save() {
        adapter.setiLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if (myFireBaseItems.size() <= 100) {
                    myFireBaseItems.add(null);
                    adapter.notifyItemInserted(myFireBaseItems.size() - 1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myFireBaseItems.remove(myFireBaseItems.size() - 1);
                            adapter.notifyItemRemoved(myFireBaseItems.size());

                            int index = myFireBaseItems.size();
                            int end = index + 10;
                            for (int i = index; i < end; i++) {
                                myFireBaseItems.add(new MyFireBaseItem(
                                        UUID.randomUUID().toString(),
                                        String.valueOf(System.currentTimeMillis())
                                ));
                            }
                            adapter.notifyDataSetChanged();
                            adapter.setLoaded();

                        }
                    }, 2000);
                } else {
                    Toast.makeText(getContext(), "Loading Done...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}