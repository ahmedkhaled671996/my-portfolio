package com.example.allinone.allUi.myUI.share;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.allinone.R;


public class ShareFragment extends Fragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_share, container, false);

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("message/rfc822");
//        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{"ahmed.khaled.z@hotmail.com"});
//        intent.putExtra(Intent.EXTRA_SUBJECT,"hi");
//        intent.putExtra(Intent.EXTRA_TEXT,"body");
//
//        try {
//            startActivity(Intent.createChooser(intent,"send mail..."));
//        }catch (ActivityNotFoundException e){
//            Toast.makeText(getContext(), "no email..", Toast.LENGTH_SHORT).show();
//        }

//        boolean b = EmailIntentBuilder.from(getContext())
//                .to("ahmed.khaled.zk@gmail.com")
//                .subject("ahmed")
//                .body("hiiiiiiiii")
//                .start();

//        final SendEmailTask sendEmailTask = new SendEmailTask();
//        sendEmailTask.execute();

        return root;
    }


//    class SendEmailTask extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            Log.i("Email sending", "sending start");
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            try {
//                GmailSender sender = new GmailSender("ahmed.khaled.z@hotmail.com", "windows.open");
//                //subject, body, sender, to
//                sender.sendMail("ahmed",
//                        "hiiiiiiiiiii",
//                        "ahmed.khaled.z@hotmail.com",
//                        "ahmed.khaled.zk@gmail.com");
//
//                Log.i("Email sending", "send");
//            } catch (Exception e) {
//                Log.i("Email sending", "cannot send");
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//        }
//    }
}
