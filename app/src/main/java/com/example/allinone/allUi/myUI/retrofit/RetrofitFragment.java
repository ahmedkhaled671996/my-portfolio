package com.example.allinone.allUi.myUI.retrofit;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.retrofit.adpters.UserAdpter;
import com.example.allinone.allUi.myUI.retrofit.apis.ApiClient;
import com.example.allinone.allUi.myUI.retrofit.apis.ApiInterface;
import com.example.allinone.allUi.myUI.retrofit.helper.EndlessRecyclerViewScrollListener;
import com.example.allinone.allUi.myUI.retrofit.modeles.User;
import com.example.allinone.customView.CustomEditText;
import com.example.allinone.customView.CustomTextView;
import com.example.allinone.hellper.RecyclerItemClickListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RetrofitFragment extends Fragment {

    private List<User> userList;
    private ApiInterface apiInterface;

    private AlertDialog.Builder userBuilder;
    private View userAlertView;
    private TextInputEditText userNameInputEditText, userEmailInputEditText;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private UserAdpter userAdpter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private CustomTextView userCount;
    private CustomEditText searchET;
    private boolean loadMoreStarter = true;

    int userPage = 1;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_retrofit, container, false);

        Snackbar.make(Objects.requireNonNull(Objects.requireNonNull(getActivity()).getCurrentFocus()),
                getString(R.string.retrofit_wiht_rx_info), Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();


        progressBar = root.findViewById(R.id.retrofitProgressBar);
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        searchET = root.findViewById(R.id.retrofit_search_ET);
        userCount = root.findViewById(R.id.retrofit_UserCount_TV);

        Retrofit retrofit = ApiClient.getApiClient();
        apiInterface = retrofit.create(ApiInterface.class);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = root.findViewById(R.id.retrofit_users_recycler_View);
        userList = new ArrayList<>();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        userAdpter = new UserAdpter(getContext(), userList);
        recyclerView.setAdapter(userAdpter);

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (isConnected(getContext())) {
                    userPage++;
                    progressBar.setVisibility(View.VISIBLE);
                    generateUsers(userPage);

                } else {
                    Toast.makeText(getContext(), "You Are Not Connected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.ClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }

            @Override
            public void onLongItemClick(View view, final int position) {


                userBuilder = new AlertDialog.Builder(getContext());
                userAlertView = getLayoutInflater().inflate(R.layout.alert_dialog_add_user, null);

                userNameInputEditText = userAlertView.findViewById(R.id.addToDBUserName);
                userNameInputEditText.setText(userList.get(position).getName());
                userEmailInputEditText = userAlertView.findViewById(R.id.addToDBUserEmail);
                userEmailInputEditText.setText(userList.get(position).getEmail());

                userBuilder.setTitle("Update & Delete user");
                userBuilder.setNegativeButton("cancel", null);
                userBuilder.setPositiveButton("update", null);
                userBuilder.setNeutralButton("delete", null);
                userBuilder.setView(userAlertView);
                final AlertDialog userDialog = userBuilder.create();
                userDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {

                        userDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (userNameInputEditText.getText().toString().equals("")
                                                || userEmailInputEditText.getText().toString().equals("")) {
                                            Toast.makeText(getContext(), "All Fields are required", Toast.LENGTH_SHORT).show();
                                        } else {
                                            updateUser(userList.get(position), position);
                                            userDialog.dismiss();
                                        }
                                    }
                                });

                        userDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (isConnected(getContext())) {
                                    delete(userList.get(position).getId(), position);
                                } else {
                                    Toast.makeText(getContext(), "You Are Not Connected", Toast.LENGTH_SHORT).show();
                                }
                                userDialog.dismiss();
                            }
                        });
                    }
                });
                userDialog.show();


            }
        }));


        root.findViewById(R.id.addUserToDBUsingRetrofit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected(getContext())) {
                    addUser();
                } else {
                    Toast.makeText(getContext(), "You Are Not Connected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (isConnected(getContext())) {
            generateUsers(userPage);
            search();
        } else {
            Toast.makeText(getContext(), "You Are Not Connected", Toast.LENGTH_SHORT).show();
        }
    }

    private void addUser() {

        userBuilder = new AlertDialog.Builder(getContext());
        userAlertView = getLayoutInflater().inflate(R.layout.alert_dialog_add_user, null);

        userNameInputEditText = userAlertView.findViewById(R.id.addToDBUserName);
        userEmailInputEditText = userAlertView.findViewById(R.id.addToDBUserEmail);

        userBuilder.setTitle("Add user");
        userBuilder.setNegativeButton("cancel", null);
        userBuilder.setPositiveButton("add", null);
        userBuilder.setView(userAlertView);
        final AlertDialog userDialog = userBuilder.create();
        userDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                userDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (userNameInputEditText.getText().toString().equals("")
                                        || userEmailInputEditText.getText().toString().equals("")) {
                                    Toast.makeText(getContext(), "All Fields are required", Toast.LENGTH_SHORT).show();
                                } else {

                                    Call<User> call = apiInterface.addUsers(
                                            UUID.randomUUID().toString(),
                                            userNameInputEditText.getText().toString(),
                                            userEmailInputEditText.getText().toString()
                                    );

                                    call.enqueue(new Callback<User>() {
                                        @Override
                                        public void onResponse(Call<User> call, Response<User> response) {
                                            Toast.makeText(getContext(), "Done... ", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onFailure(Call<User> call, Throwable t) {
                                            Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    userDialog.dismiss();
                                }
                            }
                        });
            }
        });
        userDialog.show();

    }

    public void generateUsers(final int page) {

        compositeDisposable.add(apiInterface.getUsersWithRX(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        userList.addAll(users);
                        userAdpter.notifyItemRangeChanged(userAdpter.getItemCount(), userList.size() - 1);
                        progressBar.setVisibility(View.INVISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        userCount.setText(String.valueOf(userAdpter.getItemCount()));
                    }
                })
        );

        /**
         Call<List<User>> call = apiInterface.getUsers(page);
         call.enqueue(new Callback<List<User>>() {

        @Override public void onResponse(Call<List<User>> call, Response<List<User>> response) {

        userList.addAll(response.body());
        userAdpter.notifyItemRangeChanged(userAdpter.getItemCount(),userList.size()-1);
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        userCount.setText(String.valueOf(userAdpter.getItemCount()));

        }

        @Override public void onFailure(Call<List<User>> call, Throwable t) {
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
        }

        });
         **/

    }

    public void search() {

        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!s.toString().isEmpty()) {


                    compositeDisposable.add(apiInterface.searchWithRX(s.toString())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<List<User>>() {
                                @Override
                                public void accept(List<User> users) throws Exception {
                                    userList.clear();
                                    userList.addAll(users);
                                    userAdpter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.INVISIBLE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    userCount.setText(String.valueOf(userAdpter.getItemCount()));
                                }
                            })
                    );

                    /**
                     Call<List<User>> call = apiInterface.search(s.toString());
                     call.enqueue(new Callback<List<User>>() {

                    @Override public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    userList.clear();
                    userList.addAll(response.body());
                    userAdpter.notifyDataSetChanged();
                    progressBar.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    userCount.setText(String.valueOf(userAdpter.getItemCount()));
                    }

                    @Override public void onFailure(Call<List<User>> call, Throwable t) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    });
                     **/

                } else {
                    userList.clear();
                    userCount.setText("");
                    userPage = 1;
                    generateUsers(userPage);
                }

            }
        });
    }

    public void delete(String id, final int position) {
        Call<Object> call = apiInterface.delete(id);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Toast.makeText(getContext(), "Done...", Toast.LENGTH_SHORT).show();
                userList.remove(position);
                userAdpter.notifyItemRemoved(position);
                userCount.setText(String.valueOf(userList.size()));
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUser(final User user, final int position) {

        Call<User> call = apiInterface.updateUser(
                user.getId(),
                user.getName(),
                user.getEmail()
        );

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Toast.makeText(getContext(), "Done... ", Toast.LENGTH_SHORT).show();
                userList.set(position, user);
                userAdpter.notifyItemChanged(position);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getContext(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }
}
