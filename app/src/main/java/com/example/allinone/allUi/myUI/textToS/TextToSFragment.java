package com.example.allinone.allUi.myUI.textToS;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.allinone.R;
import com.example.allinone.customView.CustomButton;
import com.example.allinone.customView.CustomEditText;

import java.util.Locale;


public class TextToSFragment extends Fragment {

    TextToSpeech textToSpeech;
    CustomEditText customEditText;
    CustomButton customButton;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_text_to_s, container, false);

        customEditText = root.findViewById(R.id.TextToSpeechEditText);
        customButton = root.findViewById(R.id.TextToSpeechButton);
        customButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textToSOnClick();
            }
        });

        return root;
    }

    public void textToSOnClick() {

        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int languageStuts = textToSpeech.setLanguage(Locale.US);
                    if (languageStuts == TextToSpeech.LANG_MISSING_DATA || languageStuts == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getContext(), "Lang is not support", Toast.LENGTH_SHORT).show();
                    } else {
                        if (customEditText.getText().toString().equals("")) {
                            int speechStuts = textToSpeech.speak("please enter some text", TextToSpeech.QUEUE_FLUSH, null);
                            if (speechStuts == TextToSpeech.ERROR) {
                                Toast.makeText(getContext(), "speak Error", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            int speechStuts = textToSpeech.speak(customEditText.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                            if (speechStuts == TextToSpeech.ERROR) {
                                Toast.makeText(getContext(), "speak Error", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                } else {
                    Toast.makeText(getContext(), "feald", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}