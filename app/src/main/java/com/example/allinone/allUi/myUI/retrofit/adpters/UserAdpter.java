package com.example.allinone.allUi.myUI.retrofit.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.allUi.myUI.retrofit.modeles.User;
import com.example.allinone.databinding.MyClass;

import java.util.List;

public class UserAdpter extends RecyclerView.Adapter<UserAdpter.UserViewHolder> {


    private List<User> users;
    private FragmentManager fragmentManager;
    private Context context;
    private LayoutInflater layoutInflater;


    public UserAdpter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        MyClass userClass = MyClass.inflate(layoutInflater, parent, false);

        return new UserViewHolder(userClass);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {

        final User user = users.get(position);
        holder.bind(user);

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void addAll(List<User> newUsers) {
        int zise = users.size();
        users.addAll(newUsers);
        notifyItemRangeChanged(zise, newUsers.size());
    }

    public String getLastItemId() {
        return users.get(users.size() - 1).getId();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        private MyClass userClass;

        public UserViewHolder(MyClass userClass) {
            super(userClass.getRoot());
            this.userClass = userClass;
        }

        public void bind(User user) {
            this.userClass.setUserModel(user);
        }

        public MyClass getUserClass() {
            return userClass;
        }
    }
}


