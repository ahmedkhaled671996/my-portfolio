package com.example.allinone.allUi.myUI.rxWithRoom.local.Database;

import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;

import java.util.List;

import io.reactivex.Flowable;

public interface IUserDatasource {

    Flowable<RoomUser> getUserById(String userId);

    Flowable<List<RoomUser>> getAllUsers();

    void insertUser(RoomUser... users);

    void updateUser(RoomUser... users);

    void deleteUser(RoomUser user);

    void deleteAllUsers();

}
