package com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.Models;

public class MyFireBaseItem {

    private String ItemID;
    private String ItemName;

    public MyFireBaseItem() {

    }

    public MyFireBaseItem(String itemID, String itemName) {
        ItemID = itemID;
        ItemName = itemName;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }
}
