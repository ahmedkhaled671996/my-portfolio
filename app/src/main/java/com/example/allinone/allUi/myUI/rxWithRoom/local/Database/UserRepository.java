package com.example.allinone.allUi.myUI.rxWithRoom.local.Database;

import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;

import java.util.List;

import io.reactivex.Flowable;

public class UserRepository implements IUserDatasource {

    private IUserDatasource mloacalDatasource;
    private static UserRepository mInstance;

    public UserRepository(IUserDatasource mloacalDatasource) {
        this.mloacalDatasource = mloacalDatasource;
    }

    public static UserRepository getInstance(IUserDatasource mloacalDatasource) {
        if (mInstance == null) {
            mInstance = new UserRepository(mloacalDatasource);
        }
        return mInstance;
    }

    @Override
    public Flowable<RoomUser> getUserById(String userId) {
        return mloacalDatasource.getUserById(userId);
    }

    @Override
    public Flowable<List<RoomUser>> getAllUsers() {
        return mloacalDatasource.getAllUsers();
    }

    @Override
    public void insertUser(RoomUser... users) {
        mloacalDatasource.insertUser(users);
    }

    @Override
    public void updateUser(RoomUser... users) {
        mloacalDatasource.updateUser(users);
    }

    @Override
    public void deleteUser(RoomUser user) {
        mloacalDatasource.deleteUser(user);
    }

    @Override
    public void deleteAllUsers() {
        mloacalDatasource.deleteAllUsers();
    }
}
