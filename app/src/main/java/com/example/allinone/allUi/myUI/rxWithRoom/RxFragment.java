package com.example.allinone.allUi.myUI.rxWithRoom;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.rxWithRoom.local.Database.UserRepository;
import com.example.allinone.allUi.myUI.rxWithRoom.local.UserDataBase;
import com.example.allinone.allUi.myUI.rxWithRoom.local.UserDatasource;
import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RxFragment extends Fragment {

    private ListView usersListView;

    private List<RoomUser> roomUsers = new ArrayList<>();
    private ArrayAdapter adapter;

    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    private AlertDialog.Builder userBuilder;
    private View userAlertView;
    private TextInputEditText userNameInputEditText, userEmailInputEditText;


    View root;

    @SuppressLint("CheckResult")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_rx, container, false);

        Snackbar.make(Objects.requireNonNull(Objects.requireNonNull(getActivity()).getCurrentFocus()),
                getString(R.string.roomWithRXInfo), Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();

        compositeDisposable = new CompositeDisposable();

        usersListView = root.findViewById(R.id.roomUsersListView);

        adapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, roomUsers);
        registerForContextMenu(usersListView);
        usersListView.setAdapter(adapter);

        UserDataBase userDataBase = UserDataBase.getInstance(getContext());
        userRepository = UserRepository.getInstance(UserDatasource.getInstance(userDataBase.userDeo()));

        loadData();

        root.findViewById(R.id.roomAdduserFab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userBuilder = new AlertDialog.Builder(getContext());
                userAlertView = getLayoutInflater().inflate(R.layout.alert_dialog_add_user, null);

                userNameInputEditText = userAlertView.findViewById(R.id.addToDBUserName);
                userEmailInputEditText = userAlertView.findViewById(R.id.addToDBUserEmail);

                userBuilder.setTitle("add user");
                userBuilder.setNegativeButton("cancel", null);
                userBuilder.setPositiveButton("add", null);
                userBuilder.setView(userAlertView);
                final AlertDialog userDialog = userBuilder.create();
                userDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        userDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (userNameInputEditText.getText().toString().equals("")
                                                || userEmailInputEditText.getText().toString().equals("")) {
                                            Toast.makeText(getContext(), "All Fields are required", Toast.LENGTH_SHORT).show();
                                        } else {
                                            addUser(new RoomUser(
                                                    UUID.randomUUID().toString(),
                                                    userNameInputEditText.getText().toString(),
                                                    userEmailInputEditText.getText().toString()
                                            ));
                                            userDialog.dismiss();

                                        }
                                    }
                                });
                    }
                });
                userDialog.show();
            }
        });

        root.findViewById(R.id.roomDelateAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAll();
            }
        });

        usersListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                userBuilder = new AlertDialog.Builder(getContext());
                userAlertView = getLayoutInflater().inflate(R.layout.alert_dialog_add_user, null);

                userNameInputEditText = userAlertView.findViewById(R.id.addToDBUserName);
                userEmailInputEditText = userAlertView.findViewById(R.id.addToDBUserEmail);

                final RoomUser user = roomUsers.get(position);
                userNameInputEditText.setText(user.getName());
                userEmailInputEditText.setText(user.getEmail());

                userBuilder.setTitle("update user");
                userBuilder.setNegativeButton("cancel", null);
                userBuilder.setPositiveButton("update", null);
                userBuilder.setView(userAlertView);
                final AlertDialog userDialog = userBuilder.create();
                userDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        userDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (userNameInputEditText.getText().toString().equals("")
                                                || userEmailInputEditText.getText().toString().equals("")) {


                                        } else {

                                            updateUser(new RoomUser(
                                                    user.getId(),
                                                    userNameInputEditText.getText().toString(),
                                                    userEmailInputEditText.getText().toString()
                                            ));
                                            userDialog.dismiss();

                                        }
                                    }
                                });
                    }
                });
                userDialog.show();


                return true;
            }
        });


        return root;
    }

    private void loadData() {

        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<RoomUser>>() {
                    @Override
                    public void accept(List<RoomUser> roomUsers) throws Exception {
                        onGetAllUsersSuccess(roomUsers);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(getContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        compositeDisposable.add(disposable);

    }

    private void onGetAllUsersSuccess(List<RoomUser> users) {
        roomUsers.clear();
        roomUsers.addAll(users);
        adapter.notifyDataSetChanged();
    }

    private void addUser(final RoomUser user) {

        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {

            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {

                userRepository.insertUser(user);
                emitter.onComplete();

            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {
                        Toast.makeText(getContext(), "Done Add User...", Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(getContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void deleteAll() {

        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {

            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {

                userRepository.deleteAllUsers();
                emitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {
                        Toast.makeText(getContext(), "delete All Users Done", Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(getContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void updateUser(final RoomUser user) {

        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {

            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {

                userRepository.updateUser(user);
                emitter.onComplete();

            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {
                        Toast.makeText(getContext(), "Done update User...", Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(getContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData();
                    }
                });
        compositeDisposable.add(disposable);

    }

}
