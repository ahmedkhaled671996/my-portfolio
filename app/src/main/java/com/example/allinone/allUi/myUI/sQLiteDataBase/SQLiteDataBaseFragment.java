package com.example.allinone.allUi.myUI.sQLiteDataBase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.sQLiteDataBase.dataBase.DataBaseControl;
import com.example.allinone.customView.CustomEditText;
import com.example.allinone.customView.CustomTextView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;


public class SQLiteDataBaseFragment extends Fragment {

    View root;

    private ArrayList<Map<String, String>> data;
    private static final int EDIT = 0, DELETE = 1, COPY = 2;
    DataBaseControl dataBaseControl;
    int itemPosition;

    CustomTextView dataView;
    CustomEditText addToDataBaseET, updateInDataBaseET_ID, updateInDataBaseET_Data, deleteFromDataBaseET;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_sqlite_database, container, false);

        Snackbar.make(Objects.requireNonNull(Objects.requireNonNull(getActivity()).getCurrentFocus()),
                getString(R.string.SQLiteDataBaseInfo), Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();

        dataBaseControl = new DataBaseControl(getContext());

        addToDataBaseET = root.findViewById(R.id.AddToDataBaseET);
        updateInDataBaseET_ID = root.findViewById(R.id.UpdateInDataBaseET_ID);
        updateInDataBaseET_Data = root.findViewById(R.id.UpdateInDataBaseET_Data);
        deleteFromDataBaseET = root.findViewById(R.id.DeleteFromDataBaseET);
        dataView = root.findViewById(R.id.SQLiteTextView);

        root.findViewById(R.id.AddToDataBaseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToDataBase();
            }
        });

        root.findViewById(R.id.UpdateInDataBaseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateInDataBase();
            }
        });

        root.findViewById(R.id.DeleteFromDataBaseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFromDataBase();
            }
        });

        root.findViewById(R.id.DeleteAllDataBaseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBaseControl.deleteAllData();
                generateNoteList();
            }
        });

        generateNoteList();

        return root;
    }


    public void generateNoteList() {


        data = new ArrayList<>();

        /**********************************************************************************/

        dataView.setText("");
        for (int i = dataBaseControl.get_All_Data().size() - 1; i >= 0; i--) {


            dataView.setText(dataView.getText() + "ـــــــــــــــــــــــــــ" + "\n" + dataBaseControl.get_All_Data().get(i).getTeastId() + "\n" + dataBaseControl.get_All_Data().get(i).getTeastData() + "\n");

        }


    }

    public void addToDataBase() {

        if (addToDataBaseET.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter some Text", Toast.LENGTH_SHORT).show();
        } else {
            dataBaseControl.insert_Test_data(String.valueOf(System.currentTimeMillis()), addToDataBaseET.getText().toString());
            generateNoteList();
            addToDataBaseET.setText("");
        }

    }

    public void deleteFromDataBase() {
        if (deleteFromDataBaseET.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter id of data", Toast.LENGTH_SHORT).show();
        } else {
            dataBaseControl.deleteData(deleteFromDataBaseET.getText().toString());
            generateNoteList();
            deleteFromDataBaseET.setText("");
        }
    }

    public void updateInDataBase() {
        if (updateInDataBaseET_ID.getText().toString().equals("")) {
            Toast.makeText(getContext(), "enter data id", Toast.LENGTH_SHORT).show();
        } else if (updateInDataBaseET_Data.getText().toString().equals("")) {
            Toast.makeText(getContext(), "enter the new data", Toast.LENGTH_SHORT).show();
        } else {
            dataBaseControl.updateData(updateInDataBaseET_ID.getText().toString(), updateInDataBaseET_Data.getText().toString());
            generateNoteList();
            updateInDataBaseET_ID.setText("");
            updateInDataBaseET_Data.setText("");
        }
    }


}