package com.example.allinone.allUi.myUI.rxWithRoom.local;

import com.example.allinone.allUi.myUI.rxWithRoom.local.Database.IUserDatasource;
import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;

import java.util.List;

import io.reactivex.Flowable;

public class UserDatasource implements IUserDatasource {

    private UserDeo userDeo;
    private static UserDatasource mInstance;

    public UserDatasource(UserDeo userDeo) {
        this.userDeo = userDeo;
    }

    public static UserDatasource getInstance(UserDeo userDeo) {

        if (mInstance == null) {
            mInstance = new UserDatasource(userDeo);
        }
        return mInstance;
    }

    @Override
    public Flowable<RoomUser> getUserById(String userId) {
        return userDeo.getUserById(userId);
    }

    @Override
    public Flowable<List<RoomUser>> getAllUsers() {
        return userDeo.getAllUsers();
    }

    @Override
    public void insertUser(RoomUser... users) {
        userDeo.insertUser(users);
    }

    @Override
    public void updateUser(RoomUser... users) {
        userDeo.updateUser(users);
    }

    @Override
    public void deleteUser(RoomUser user) {
        userDeo.deleteUser(user);
    }

    @Override
    public void deleteAllUsers() {
        userDeo.deleteAllUsers();
    }

}
