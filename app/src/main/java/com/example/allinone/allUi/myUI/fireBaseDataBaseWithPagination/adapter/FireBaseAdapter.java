package com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.Models.MyFireBaseItem;
import com.example.allinone.customView.CustomTextView;

import java.util.ArrayList;
import java.util.List;

public class FireBaseAdapter extends RecyclerView.Adapter<FireBaseAdapter.MyViewHolder> {


    List<MyFireBaseItem> myFireBaseItems;
    Context context;

    public FireBaseAdapter(Context context) {
        this.myFireBaseItems = new ArrayList<>();
        this.context = context;
    }

    public void addAll(List<MyFireBaseItem> newMyFireBaseItems) {
        int initSize = myFireBaseItems.size();
        myFireBaseItems.addAll(newMyFireBaseItems);
        notifyItemRangeChanged(initSize, newMyFireBaseItems.size());
    }

    public String getLataItemId() {
        return myFireBaseItems.get(myFireBaseItems.size() - 1).getItemID();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_firebase_recyclerview, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.itemID.setText(myFireBaseItems.get(position).getItemID());
        holder.itemName.setText(myFireBaseItems.get(position).getItemName());
    }

    @Override
    public int getItemCount() {
        return myFireBaseItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CustomTextView itemID, itemName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemID = itemView.findViewById(R.id.firebase_item_id_TV);
            itemName = itemView.findViewById(R.id.firebase_item_name_TV);
        }
    }
}
