package com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allinone.R;
import com.example.allinone.allUi.myUI.fireBaseDataBaseWithPagination.Models.MyFireBaseItem;
import com.example.allinone.customView.CustomTextView;

import java.util.List;

public class SweperAdapter extends RecyclerView.Adapter<SweperAdapter.MyviewHolder> {

    private Context context;
    private List<MyFireBaseItem> myFireBaseItems;

    public SweperAdapter(Context context, List<MyFireBaseItem> myFireBaseItems) {
        this.context = context;
        this.myFireBaseItems = myFireBaseItems;
    }

    public void addAll(List<MyFireBaseItem> newMyFireBaseItems) {
        int initSize = myFireBaseItems.size();
        myFireBaseItems.addAll(newMyFireBaseItems);
        notifyItemRangeChanged(initSize, newMyFireBaseItems.size());
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_swibe_to_delete, parent, false);


        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {

        MyFireBaseItem fireBaseItem = myFireBaseItems.get(position);
        holder.name.setText(fireBaseItem.getItemName());
    }

    @Override
    public int getItemCount() {
        return myFireBaseItems.size();
    }

    public void removeItem(int position) {
        myFireBaseItems.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(MyFireBaseItem myFireBaseItem, int position) {
        myFireBaseItems.add(position, myFireBaseItem);
        notifyItemInserted(position);
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        public CustomTextView name;
        public ConstraintLayout background, forground;


        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.firebase_name_TV);
            background = itemView.findViewById(R.id.view_background);
            forground = itemView.findViewById(R.id.view_foreground);


        }
    }
}
