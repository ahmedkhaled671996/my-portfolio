package com.example.allinone.allUi.myUI.rxWithRoom.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.allinone.allUi.myUI.rxWithRoom.models.RoomUser;

import static com.example.allinone.allUi.myUI.rxWithRoom.local.UserDataBase.DATABASE_VERSION;

@Database(entities = RoomUser.class, version = DATABASE_VERSION)
public abstract class UserDataBase extends RoomDatabase {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "zsnuw_database_room";

    public abstract UserDeo userDeo();

    private static UserDataBase mInstance;

    public static UserDataBase getInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context, UserDataBase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return mInstance;
    }
}
