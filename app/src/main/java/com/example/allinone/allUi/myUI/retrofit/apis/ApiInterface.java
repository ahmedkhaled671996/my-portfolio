package com.example.allinone.allUi.myUI.retrofit.apis;

import com.example.allinone.allUi.myUI.retrofit.modeles.User;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("getUsers.php")
    Call<List<User>> getUsers(@Query("page") int pageNumber);

    @GET("getUsers.php")
    Observable<List<User>> getUsersWithRX(@Query("page") int pageNumber);

    @POST("raedContacts.php")
    Call<List<User>> getAllUsers();

    @POST("raedContacts.php")
    Observable<List<User>> getAllUsersWithRX();

    @FormUrlEncoded
    @POST("writeToDB.php")
    Call<User> addUsers(@Field("id") String id, @Field("name") String name, @Field("email") String email);

    @FormUrlEncoded
    @POST("update.php")
    Call<User> updateUser(@Field("id") String id, @Field("name") String name, @Field("email") String email);

    @GET("search.php")
    Call<List<User>> search(@Query("name") String name);

    @GET("search.php")
    Observable<List<User>> searchWithRX(@Query("name") String name);

    @GET("Delete.php")
    Call<Object> delete(@Query("id") String id);


}
