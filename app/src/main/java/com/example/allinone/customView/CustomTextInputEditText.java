package com.example.allinone.customView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;


@SuppressLint("AppCompatCustomView")
public class CustomTextInputEditText extends TextInputEditText {

    private final String FONT_NAME = "jf_flat_regular.ttf";

    public CustomTextInputEditText(Context context) {
        super(context);
        init();
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), FONT_NAME);
        setTypeface(font);

    }
}
